class HomePage {
    getUserName() {
        return cy.get('input[name="login"]');
    }
    getPassword(){
        return cy.get('input[name="password"]');
    }
   
    getLoginButton() {
        return cy.get('button[type=submit]');
    }
   
    getMenuBarButton(){

        return cy.get('body > header > nav > ul.o_menu_apps > li > a');
    }
}
    export default HomePage