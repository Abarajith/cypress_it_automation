class MenuPage {

    getMenuBarButton(){

        return cy.get('body > header > nav > ul.o_menu_apps > li > a');
    }

   getBudgetAppButton (){

    return cy.get('body > header > nav > ul.o_menu_apps > li > div > a:nth-child(18) > img');
   }
}

export default MenuPage