// type definitions for Cypress object "cy"
// <reference types="cypress" />


import HomePage from '../../support/PageObjects/HomePage';
import {Given,Then}  from "cypress-cucumber-preprocessor/steps";

const homePage=new HomePage();

Given('I open the login Page in Sulaba Website',  ( )=> {
    cy.visit(Cypress.env('url'));
     
    Then('I do the login by entering username and password', () =>  {
    
    cy.fixture("user").then((user)=>{

    const Username = user.Username;
    const Password = user.password;

    homePage.getUserName().type(Username);
    homePage.getPassword().type(Password);

    })    
    
    })
     
    Then ('I do the assertions for login Button and login successfully',function() {
    //homePage.getRegisterButton().should('have.attr','disabled','disabled');
    //homePage.getRegisterButton().should('be.disabled');
    //homePage.getPassword().type(NewPassword);
    homePage.getLoginButton().click();
    })
     
})