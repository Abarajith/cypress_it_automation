
import HomePage from  "../../support/PageObjects/HomePage";
import MenuPage from "../../support/PageObjects/MenuPage";


const homePage = new HomePage();
const MenuPage = new MenuPage();

Given('I open the login Page in Sulaba Website', function () {
  cy.visit("https://purchase.sushumna.isha.in/web/login");
});

//Then('the logo name should be cypress.io', function () {
  //cy.get('.navbar-brand').should('have.text', "cypress.io");
//});

Then('I do the login by entering username and password', function () {
  
  cy.fixture("user").then((user)=>{

    const Username = user.username;
    const Password = user.password;

    homePage.getUserName().type(Username);
    homePage.getPassword().type(Password);

  })
  
  
 
});

Then('I do the assertions for login Button and login successfully', function () {
  //cy.get('button[type=submit]').click({ multiple: true, force: true });
  homePage.getLoginButton().click({ multiple: true, force: true });
  //cy.get('.navbar-brand').should('have.text', "cypress.io");
});

Then('I wait for the page to reload',function (){

  cy.reload();
})

Then ('I click on the Budget App button',function (){

  MenuPage.getBudgetAppButton().click({ multiple: true, force: true });
})
Then('I click the MenuBar button',function (){

  MenuPage.getMenuBarButton().click({ multiple: true, force: true });

})
